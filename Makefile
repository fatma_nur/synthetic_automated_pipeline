# Shell settings
SHELL       := /bin/bash
.SHELLFLAGS := -e -u -c

# Use a single shell
.ONESHELL:

# So we can use $$(variable) on the prerequisites, that expand at matching time.
.SECONDEXPANSION:

# Default target
all:

# Use it as a dependency to force a target to always rebuild
FORCE:

LAST_ANALYSIS = $(patsubst classification/%,%,$(lastword $(sort $(wildcard classification/*))))


.PHONY: inputs run clean check clean review review-all deploy

inputs:
	@rosrun progress_monitoring pipeline run --stage-type Input .

run:
	@rosrun progress_monitoring pipeline run .

check:
	@rosrun progress_monitoring pipeline check-dag .

clean:
	@rosrun progress_monitoring pipeline clean .

review:
	@printf "Analysis: %s\n" "$(LAST_ANALYSIS)"
	rosrun model_classification feature-labeller --pipeline . --survey $(LAST_ANALYSIS) --low-confidence --new

review-all:
	@printf "Analysis: %s\n" "$(LAST_ANALYSIS)"
	rosrun model_classification feature-labeller --pipeline . --survey $(LAST_ANALYSIS) --low-confidence

deploy:
	@rosrun data_delivery frontend_deploy -v 2 -k s3 -c config/frontend_deploy.ini frontend/$(LAST_ANALYSIS)
	@rosrun data_delivery frontend_deploy -v 2 -k db -c config/frontend_deploy.ini frontend/$(LAST_ANALYSIS)
